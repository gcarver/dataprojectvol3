def Generate_windows(self, n):
    """
    Generate n*n new windows using the new_window function n*n times
    Example: n==2 => 4 new windows
    Parameters:
        n: (int), number of windows
    """

    # Get the boundaries of the original window
    left = self.lon_left
    right = self.lon_right
    low = self.lat_low
    high = self.lat_high

    # Find step size for window slice
    lon_slice_step = (left - right)/n
    lat_slice_step = (low - high)/n
    
    # Find new edges for right and low
    new_right = left + lon_slice_step
    new_low = low + lat_slice_step

    # Create new windows without breaking the boundaries of the original box
    window_num = 0
    while low <= new_low:
        while right >= new_right:
            window_num += 1

            # Set bounding box
            bbox = [left, new_right, new_low, high]

            # Create new window
            self.new_window(bbox, window_num)

            # Update Longitude boundaries (move right)
            left = new_right
            new_right = new_right + lon_slice_step

        # Go back to first longitude slice
        left = self.lon_left   
        new_right = left + lon_slice_step

        # Update Latitude boundaries (move down)
        low = new_low
        new_low = new_low + lat_slice_step