	def generate_random_walkers(self, iters = 1,m=10, steps = 1000, plot = False, p_idx = False, proportion = 0.2):
		if plot:
			self.plot(self.lons,self.lats,self.topo)

		xpt = (self.lon_right + self.lon_left) / 2
		ypt = (self.lat_high + self.lat_low) / 2
		center = np.array([xpt,ypt])

		eps = np.random.random(m) * 0.2

		start = []
		end = []

		for j in range(iters):
			for i, e in enumerate(eps):
				if p_idx:
					print(i,j)
				point = np.array([center[0] + self.rand_sign() * e, center[1] +self.rand_sign() *e])
				start.append((point[0], point[1]))
				x,y = self.random_walk(point,steps, proportion = proportion)
				if plot:
					plt.plot(x,y, label = str(j) + str(i)  )
				end.append((x[-1], y[-1]))

		return start,end
		
	def plot_degree_slope(self):
		new_topo = np.degrees(np.arctan(self.grady / self.gradx))
		fig      = plt.figure()
		ax       = fig.add_subplot(111)
		p        = ax.contourf(self.lons, self.lats, new_topo, 80, alpha=0.5)
		colp     = plt.colorbar(p)

	def grad_vec(self):
		point = np.array([-108,38])
		fx    = self.fx_lookup(point)
		fy    = self.fy_lookup(point)
		print('fx,fy',fx,fy)
		print('sign',np.sign(fx), np.sign(fy))
		theta  = np.arctan(abs(fy)/abs(fx))
		print('theta', theta, np.degrees(theta))
		mag    = np.sqrt((fx / self.xkm_deg)**2 + (fy / self.ykm_deg)**2)
		print('mag',mag)
		#mag = 1
		point2 = np.array([point[0] + np.sign(fx) * np.cos(theta)* mag, point[1] + np.sign(fy) * np.sin(theta) * mag])
		print('points',point,point2)

		fig = plt.figure()
		ax  = fig.add_subplot(111)
		p   = ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
		plt.xlabel('lons',fontsize=16)
		plt.ylabel('lats',fontsize=16)
		colp = plt.colorbar(p)
		colp.set_label('Altitude')
		plt.plot([point[0], point2[0]], [point[1], point2[1]], '-r')

	def random_walk2(self, start, steps = 10000):
		# defining the number of steps
		n = steps
		#start = [0,0]
		bbox = self.bbox
		x = np.zeros(n)
		y = np.zeros(n)

		x[0] = start[0]
		y[0] = start[1]

		xstep = self.xeps
		ystep = self.yeps

		# filling the coordinates with random variables
		for i in range(1, n):
			dir_ = np.random.random() * 2 * np.pi #Random angle in radians
			x[i] = x[i - 1] + xstep * np.cos(dir_)
			y[i] = y[i - 1] + ystep * np.sin(dir_)
			if x[i] <= bbox[0]:
				x[i] = x[i - 1] - xstep * np.cos(dir_)
			if x[i] >= bbox[1]:
				x[i] = x[i - 1] - xstep * np.cos(dir_)
			if y[i] <= bbox[2]:
				y[i] = y[i - 1] - ystep * np.sin(dir_)
			if y[i] >= bbox[3]:
				y[i] = y[i - 1] - ystep * np.sin(dir_)
		#plt.plot(x,y)
		#plt.show()
		return x,y

	def test_rand_walkers(self, n, bboxes, starts, steps = 10000):
		fig = plt.figure()
		ax  = fig.add_subplot(111)

		for j, zone in enumerate(bboxes):
			for i in range(n):
				x,y = self.random_walk2(starts[j], steps)
				if x[-1] >= zone[0] and x[-1] <= zone[1] and y[-1] >= zone[2] and y[-1] <= zone[3]:
					print(j,'True')
				print(x[-1],y[-1])
				plt.plot(x,y, label = str(i) + str(j) )

		ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
		plt.legend()
		plt.show()

	def dist(self, y1, y2):
		i, j  = self.index_lookup(y1)
		k, l  = self.index_lookup(y2)
		z1    = self.topo[i][j]
		z2    = self.topo[k][l]
		dist  = z1 - z2
		#print(dist)
		diag1 = np.sqrt( ((abs(y1[0]) - abs(y2[0])) * self.xkm_deg)**2 +((abs(y1[1]) - abs(y2[1])) * self.ykm_deg)**2  )
		return diag1, dist
		#return np.sqrt(diag1**2 + dist**2)

	def view_area(self,center, n, height = 0.00175, lines = False):
		fig = plt.figure()
		ax  = fig.add_subplot(111)
		p   = ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
		plt.xlabel('lons',fontsize=16)
		plt.ylabel('lats',fontsize=16)
		colp = plt.colorbar(p)
		colp.set_label('Altitude')

		thetas =  np.linspace(0,2*np.pi, n)
		x1, y1 = [], []
		h = self.topo_lookup(center) + height #average height of person in kilometers
		for theta in thetas:
			out = []
			i = 1
			out.append(center)
			z = self.topo_lookup(out[0])
			while h >= z and out[i-1][0] > self.lon_left and out[i-1][0] < self.lon_right and out[i-1][1] > self.lat_low and out[i-1][1] < self.lat_high :
				out.append( np.array([ out[i-1][0] + np.cos(theta) * self.xeps , out[i-1][1] + np.sin(theta)* self.yeps]) )
				z = self.topo_lookup(out[i])
				i += 1
			x,y = list(zip(*out))
			if lines:
				plt.plot(x,y,'-r')
			x1.append(x[-1])
			y1.append(y[-1])
		if not lines:
			plt.plot(center[0], center[1], '.r')
		plt.plot(x1, y1 , '-b')
		plt.show()

	def line_path(self, center, hr, theta, p_loop = False, p_dist=False, plot = False):
		m = int(hr / self.dt)

		eps = 0.1

		out = []
		ztotal = 0
		dist = 0
		i = 1
		out.append(center)
		speeds = []
		#for k in range(m):
		while hr > 0:
			index1, index2 = self.index_lookup(out[i-1])
			vel = self.func(self.gradient[index1][index2])
			speeds.append(vel)
			if p_loop:
				print('vel {:.4} {:.4} ({:.4}, {:.4}) ({:.4}, {:.4}) {:.4} {:.4}'.format(xvel, yvel, str(fx), str(fy), inx, iny , str(out[i-1][0]), str(out[i-1][1]) ) )
			#out.append( np.array([ out[i-1][0] + np.cos(theta) * vel * self.dt / self.xkm_deg  , out[i-1][1] + np.sin(theta)* vel * self.dt / self.ykm_deg ]) )
			out.append( np.array([ self.lon_lookup(out[i-1][0] + np.cos(theta)*self.xeps)   , self.lat_lookup(out[i-1][1] + np.sin(theta)*self.yeps) ]) )
			hr -= self.yeps * self.ykm_deg / vel
			y1 = out[i-1]
			y2 = out[i]
			z1 = self.topo_lookup(y1)
			z2 = self.topo_lookup(y2)
			zdist = z1 - z2
			diag1 = np.sqrt( ((abs(y1[0]) - abs(y2[0])) * self.xkm_deg)**2 +((abs(y1[1]) - abs(y2[1])) * self.ykm_deg)**2  )
			dist += np.sqrt(diag1**2 + zdist**2)
			ztotal += abs(zdist)
			i += 1

		if p_dist:
			print(i,'total',dist, 'ztotal', ztotal)

		x,y = list(zip(*out))

		if plot:
			fig = plt.figure()
			ax  = fig.add_subplot(111)
			p   = ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
			plt.xlabel('lons',fontsize=16)
			plt.ylabel('lats',fontsize=16)
			colp = plt.colorbar(p)
			colp.set_label('Altitude')

			points = np.array([x, y]).T.reshape(-1, 1, 2)
			segments = np.concatenate([points[:-1], points[1:]], axis=1)
			lc = LineCollection(segments, cmap=plt.get_cmap('Reds'), norm=plt.Normalize(0, 5))
			lc.set_array(np.array(speeds))
			lc.set_linewidth(1)
			q = plt.gca().add_collection(lc)
			colq = plt.colorbar(q)
			colq.set_label('Speed')

			plt.show()

		return x,y, speeds

	def hours_after(self, center, hr, n, lines = False, p_loop = False, p_dist = False, plot = False):
		if plot:
			fig = plt.figure()
			ax = fig.add_subplot(111)
			p = ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
			plt.xlabel('lons',fontsize=16)
			plt.ylabel('lats',fontsize=16)
			colp = plt.colorbar(p)
			colp.set_label('Altitude')

		m = int(hr / self.dt)

		eps = 0.1

		max_ = np.log(4.8)
		min_ = np.log(0.1)
		speed = np.log(3.2)

		g = lambda fx: max(min_, min(max_, speed - eps * fx))
		h = lambda fy: max(min_, min(max_, speed - eps * fy))

		#f = lambda x:  3.2 * (1 - 1 / (1 + np.exp(-x/self.gradx.max())))

		f = lambda x: -np.exp(max(np.log(0.1), min(np.log(4.8), np.log(3.2) + np.sign(x) * abs(np.log(1 + abs(x)))) ))

		thetas =  np.linspace(0,2*np.pi, n)
		x3, y3 = [], []
		for j,theta in enumerate(thetas):
			x,y,speeds = self.line_path(center, hr, theta)
			x3.append(x[-1])
			y3.append(y[-1])

			if lines and plot:
				points = np.array([x, y]).T.reshape(-1, 1, 2)
				segments = np.concatenate([points[:-1], points[1:]], axis=1)
				lc = LineCollection(segments, cmap=plt.get_cmap('Reds'), norm=plt.Normalize(0, 5))
				lc.set_array(np.array(speeds))
				lc.set_linewidth(1)
				q = plt.gca().add_collection(lc)

		if lines and plot:
			colq = plt.colorbar(q)
			colq.set_label('Speed')
		if plot:
			plt.plot(x3, y3 , '-b')
			plt.plot(center[0],center[1], '.r')
			plt.show()
		return x3,y3

	def radius(self, center, r, n, lines = False, plot = False):
		#center = np.array([self.lon_lookup(center[0]), self.lat_lookup(center[1])])
		if plot:
			fig = plt.figure()
			ax = fig.add_subplot(111)
			plt.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
			plt.xlabel('lons',fontsize=16)
			plt.ylabel('lats',fontsize=16)

		thetas =  np.linspace(0,2*np.pi, n)
		x1, y1 = [], []
		for theta in thetas:
			out = []
			zdist = 0
			dist = 0
			i = 1
			out.append(center)
			while dist <= r:
				out.append( np.array([ out[i-1][0] + np.cos(theta) * self.xeps , out[i-1][1] + np.sin(theta)* self.yeps]) )
				xy, z = self.dist(out[i-1], out[i])
				dist += np.sqrt(xy**2 + z**2)
				zdist += abs(z)
				#dist += self.dist(out[i-1], out[i])
				i += 1
			#print('total',dist)
			#print('z',zdist)
			x,y = list(zip(*out))
			if lines and plot:
				plt.plot(x,y,'-r')
			x1.append(x[-1])
			y1.append(y[-1])
		if plot:
			plt.plot(x1, y1 , '-b')
			plt.show()
		return x1,y1

	def linear_dist(self, y1, y2, p_loop = False, p_dist = False):
		fig = plt.figure()
		ax = fig.add_subplot(111)
		plt.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
		plt.xlabel('lons',fontsize=16)
		plt.ylabel('lats',fontsize=16)

		numer = abs(y2[1] - y1[1])
		denom = abs(y2[0] - y1[0])

		if numer is not 0 and denom is not 0:
			slope = numer / denom

		signy = np.sign(y1[1] - y2[1])
		signx = np.sign(y1[0] - y2[0])

		out = []
		dist = 0
		out.append(y1)
		i = 0
		tol = 1e-2
		zdist = 0

		while np.all(np.abs(out[i] - y2) > tol):
			if p_loop:
				print('loop', out[i] - y2, tol, np.all(out[i] - y2) )
			out.append(np.array([ out[i-1][0] + signx * self.xeps , out[i-1][1] + signy * slope* self.yeps]))
			xy, z = self.dist(out[i-1], out[i])
			dist += np.sqrt(xy**2 + z**2)
			zdist += abs(z)
			i +=1
		if p_dist:
			print('dist', dist, 'zdist', zdist)
		x,y = list(zip(*out))
		plt.plot(x, y  , '-r')
		plt.plot(y1[0],y1[1], '.r')
		plt.plot(y2[0],y2[1] , '.b')
		plt.show()

	def downhill(self, t, y):
		i,j = self.index_lookup(y)
		z = self.topo[i][j]
		fx = self.gradx[i][j]
		fy = self.grady[i][j]

		fx = np.sign(fx)
		fy = np.sign(fy)

		new_pos = np.array([self.lon_lookup(y[0] - self.xeps * fx), self.lat_lookup(y[1] - self.yeps * fy)])
		return new_pos

	def run_downhill(self,start, steps, filename, ani = False):
		start = np.array([self.lon_lookup(start[0]), self.lat_lookup(start[1])])
		out = np.zeros((steps,2))
		out[0] = start

		for i in range(1, steps):
			out[i] = self.downhill(0, out[i-1])

		if not ani:
			self.plot_path(out)
		if ani:
			self.ani_path(out, start, filename )

	def ani_path(self, input_, start, filename = 'ani.mp4'):
		x = input_[:,0]
		y = input_[:,1]
		fig = plt.figure()
		ax = fig.add_subplot(111)
		ax.contourf(self.lons, self.lats, self.topo, 80, alpha=0.5)
		ax.set_xlabel('X')
		ax.set_ylabel('Y')
		ax.set_ylim((self.lat_low,self.lat_high))
		ax.set_xlim((self.lon_left,self.lon_right))
		ax.set_title('start = {}'.format(start))

		lorenz_1 = plt.plot([], [], '-r', alpha = 0.5, label='Solution 1')[0]

		def update(i):
			lorenz_1.set_data(x[:i+1], y[:i+1])
			return lorenz_1

		# Animate and save
		ani = animation.FuncAnimation(fig, update, frames = range(100), interval = 25)
		ani.save(filename, bitrate = 1000)



	def plot_path(self, input_, levels = 80, alpha=0.5, title = 'PATH'):
		fig = plt.figure()
		ax = fig.add_subplot(111)
		plt.contourf(self.lons, self.lats, self.topo, levels, alpha=alpha)
		plt.xlabel('lons',fontsize=16)
		plt.ylabel('lats',fontsize=16)
		plt.plot(input_[:,0], input_[:,1]  , '-r')
		plt.title(title)


	def compare_hrs_radius(self,center, hr, radius, n = 20):
		x3,y3 = self.hours_after(center, hr, n)
		x1, y1 = self.radius(center,radius, n)

		self.plot(self.lons,self.lats,self.topo, title='Compare radius and hours_after')
		plt.plot(x3,y3, '-b', label = 'Time dependent radius')
		plt.plot(x1,y1, '-r', label = 'Dist over topography')
		plt.legend()

	def compare_walk_speed(self):
		poly = self.fit_cheby(18,plot = True, p_xy = True)
		d = np.linspace(-90,90, 1000)
		out = []
		for i in d:
		    out.append(self.func(i))
		plt.rcParams['figure.figsize'] = [5, 5]
		plt.plot(d,out, label='Final Function' )
		plt.title('Interpolation of fabricated walking speeds')
		plt.xlabel('Degree Slope (- Downhill, + Uphill)')
		plt.ylabel('Walking Speed km/hr')
		plt.legend()
		plt.show()

			def test_rand_walkers2(self, n, steps = 1000):
		self.plot(self.lons,self.lats,self.topo)

		m = 10

		center = np.array([-107.45,37.45])
		eps = np.random.random(m) * 0.2

		start = []
		end = []

		for j in range(n):
			for i, e in enumerate(eps):
				print(i,j)
				point = np.array([center[0] + self.rand_sign() * e, center[1] +self.rand_sign() *e])
				start.append((point[0], point[1]))
				x,y = self.random_walk(point,steps)
				plt.plot(x,y, label = str(j) + str(i)  )
				end.append((x[-1], y[-1]))

		"""
		for j, zone in enumerate(bboxes):
			for i in range(n):
				x,y = self.random_walk(starts[j], steps)
				if x[-1] >= zone[0] and x[-1] <= zone[1] and y[-1] >= zone[2] and y[-1] <= zone[3]:
					print(j,'True')
				print(x[-1],y[-1])
				plt.plot(x,y, label = str(i) + str(j) )

		"""

		#plt.legend()
		plt.show()

		return start,end

