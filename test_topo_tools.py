from topo_tools import topo_tools

def make_obj():
	topo_file = 'OG_topo.npy'
	lons_file = 'OG_lons.npy'
	lats_file = 'OG_lats.npy'


	topo_OBJ = topo_tools(topo_file, lons_file, lats_file)
	return topo_OBJ

def test_new_window():
	topo_OBJ = make_obj()
	bbox = topo_OBJ.bbox

	topo_OBJ.new_window(bbox, 6)

	# print(bbox)

	# bbox[0] += 0.1
	# bbox[1] -= 0.1
	# bbox[2] += 0.1
	# bbox[3] -= 0.1

	# topo_OBJ.new_window(bbox, 6)

